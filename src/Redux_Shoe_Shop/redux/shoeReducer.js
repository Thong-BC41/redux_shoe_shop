import { data_shoe } from "../dataShoe";

let initialValue = {
  listShoe: data_shoe,
  cart: [],
};

export const shoeReducer = (state = initialValue, action) => {
  switch (action.type) {
    case "ADD_TO_CART": {
      let newCart = [...state.cart];
      let index = newCart.findIndex((item) => {
        return item.id == action.payload.id;
      });
      if (index == -1) {
        let newShoe = { ...action.payload, soluong: 1 };
        newCart.push(newShoe);
      } else {
        newCart[index].soluong++;
      }
      return { ...state, cart: newCart };
    }
    case "CHANGE_QUANTITY": {
      let newCart = [...state.cart];
      let index = newCart.findIndex((item) => {
        return item.id == action.payload.idShoe;
      });
      newCart[index].soluong = newCart[index].soluong + action.payload.luachon;
      if (newCart[index].soluong >= 1) {
        return { ...state, cart: newCart };
      } else {
        let cloneCart = state.cart.filter((item) => {
          return item.id != action.payload.idShoe;
        });
        return { ...state, cart: cloneCart };
      }
    }
    case "DELETE": {
      let newCart = state.cart.filter((item) => {
        return item.id != action.payload;
      });
      return { ...state, cart: newCart };
    }
    default:
      return state;
  }
};
