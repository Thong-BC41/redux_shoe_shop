import React, { Component } from "react";
import { connect } from "react-redux";

class CartShoe extends Component {
  renderTrbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img style={{ width: 50 }} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleChangQuantity(item.id, -1);
              }}
              className="btn btn-dark mx-3"
            >
              -
            </button>
            <strong> {item.soluong}</strong>
            <button
              onClick={() => {
                this.props.handleChangQuantity(item.id, +1);
              }}
              className="btn btn-dark mx-3"
            >
              +
            </button>
          </td>
          <td>{item.soluong * item.price}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleDelete(item.id);
              }}
              className="btn btn-primary"
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <h2>CartShoe</h2>
        <table className="table">
          <thead>
            <th>ID</th>
            <th>Name</th>
            <th>Hình Ảnh</th>
            <th>Số Lượng</th>
            <th>Price</th>
            <th>Action</th>
          </thead>
          <tbody>{this.renderTrbody()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToprops = (state) => {
  return {
    cart: state.shoeReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleChangQuantity: (idShoe, luachon) => {
      let action = {
        type: "CHANGE_QUANTITY",
        payload: { idShoe: idShoe, luachon },
      };
      dispatch(action);
    },
    handleDelete: (idShoe) => {
      let action = {
        type: "DELETE",
        payload: idShoe,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToprops, mapDispatchToProps)(CartShoe);
