import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

class ListShoe extends Component {
  render() {
    return (
      <div>
        <h2>ListShoe</h2>
        <div className="row">
          {this.props.list.map((item) => {
            return <ItemShoe shoe={item} />;
          })}
        </div>
      </div>
    );
  }
}

let mapStateToprops = (state) => {
  return {
    list: state.shoeReducer.listShoe,
  };
};

export default connect(mapStateToprops)(ListShoe);
