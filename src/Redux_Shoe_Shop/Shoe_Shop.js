import React, { Component } from "react";
import CartShoe from "./CartShoe";
import ListShoe from "./ListShoe";

export default class Shoe_Shop extends Component {
  render() {
    return (
      <div>
        <h2 style={{ backgroundColor: "red" }}> SHOE_SHOP</h2>
        <div>
          <div>
            <CartShoe />
          </div>
          <div>
            <ListShoe />
          </div>
        </div>
      </div>
    );
  }
}
